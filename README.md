# flampy
Flampy - самодельная лампа дружбы, основанная на STM32

## Внимание!
Данный проект находится в разработке, и репозиторий будет пополняться постепенно. Текущий прогресс всегда можно найти в конце README в разделе [#Прогресс](#Прогресс) ฅ=^w^=ฅ

## Электронные компоненты
- STM32F103C8T6
- ESP8266
- DC-DC Booster XL6009
- RGB светодиод 10W
- RGB светодиоды 3528, 3шт
- транзистор NPN КТ315*
- транзистор PNP КТ209*, 3шт
- резисторы 220, 1к, 1М
- переключатели, 2шт
- W25X20CLSNIG/REEL SPI Flash 2мбит
- Micro-USB разъём

## Прогресс
- [ ] разработка дизайна
- [ ] ожидание комплектующих **(5/13)**
- [x] датчик касания
- [x] [сервер](//github.com/hikiko4ern/flampy-server) для связи ламп
