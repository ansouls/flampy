#include "stm32f1xx_hal.h"

#include "config.h"
#include "uart.h"

uint32_t Sensor_Config(ADC_HandleTypeDef* hadc, UART_HandleTypeDef huart) {
	uint32_t adc = 0, touch;
	
	for(int i=0; i < AMOUNT; i++) {
		adc += HAL_ADC_GetValue(hadc);
		HAL_Delay(1000/AMOUNT);
	}
	
	uint32_t avr = adc / AMOUNT;
  
  UART_SendString(huart, "\n\r\tdefault avr: "); UART_SendNumber(huart, avr); UART_SendNL(huart);
	
  UART_SendString(huart, "\n\r\t>>> ! Touch sensor ! <<<\n\n\r");
  
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_15);
	HAL_Delay(5000);
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_15);
	
	uint8_t cnt = 0;
	uint32_t sum = 0;
	for(int i=0; i < AMOUNT; i++) {
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_15);
		
		HAL_ADC_Start(hadc);
		HAL_ADC_PollForConversion(hadc, 100);
		adc = HAL_ADC_GetValue(hadc);
		
		if (adc > avr) {
			sum += adc;
			cnt++;
		}
		
		HAL_ADC_Stop(hadc);
		
		HAL_Delay(1000/AMOUNT);
	}
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);
	
	float del = 0.5;
	
	do {
		touch = sum / cnt * del;
		del += 0.1;
	} while (touch <= avr);
  
  return touch;
}

uint8_t Sensor_State(ADC_HandleTypeDef* hadc, uint32_t touch, UART_HandleTypeDef huart) {
  uint8_t cnt = 0;
		
	for(int i=0; i < AMOUNT; i++) {
		HAL_ADC_Start(hadc);
		HAL_ADC_PollForConversion(hadc, 100);
		uint32_t adc = HAL_ADC_GetValue(hadc);
		HAL_ADC_Stop(hadc);
		
		UART_SendNumber(huart, adc); UART_SendSymbol(huart, ' ');
		
		if (adc >= touch) cnt++;
		
		HAL_Delay(DELAY/AMOUNT);
	}
	
  return (cnt >= COUNTER_ON) ? 1 : 0;
}
