#include "stm32f1xx_hal.h"

void UART_SendSymbol(UART_HandleTypeDef huart, uint8_t data) {
  while(!(huart.Instance->SR & USART_SR_TC));
  huart.Instance->DR = data;
}

void UART_SendString(UART_HandleTypeDef huart, char* str) {
  uint8_t i=0;
  while(str[i]) {
    UART_SendSymbol(huart, str[i]);
    i++;
  }
}

void UART_SendNumber(UART_HandleTypeDef huart, int data) {
  char value[10];
  int i = 0;
  
  do {
    value[i++] = (char)(data % 10) + '0';
    data /= 10;
  } while(data);
  
  while(i) {
    UART_SendSymbol(huart, value[--i]); 
  }
}

void UART_SendNL(UART_HandleTypeDef huart) {
	UART_SendString(huart, "\n\r");
}
