#ifndef CONFIG_H
#define CONFIG_H

#define DELAY 500
#define AMOUNT 10
#define COUNTER_ON 2

#define UART_INTERFACE huart1
#define SENSOR_INTERFACE hadc2

#define SETTINGS 0x800FC00

#endif
