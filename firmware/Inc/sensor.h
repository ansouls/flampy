#ifndef SENSOR_H
#define SENSOR_H

uint32_t Sensor_Config(ADC_HandleTypeDef*, UART_HandleTypeDef);
uint8_t Sensor_State(ADC_HandleTypeDef*, uint32_t, UART_HandleTypeDef);

#endif
