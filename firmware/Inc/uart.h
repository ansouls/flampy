#ifndef UART_H
#define UART_H

void UART_SendSymbol(UART_HandleTypeDef, uint8_t);

void UART_SendString(UART_HandleTypeDef, char*);

void UART_SendNumber(UART_HandleTypeDef, int);

void UART_SendNL(UART_HandleTypeDef);
 
#endif
